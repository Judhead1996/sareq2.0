import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SardisPage } from './sardis.page';

describe('SardisPage', () => {
  let component: SardisPage;
  let fixture: ComponentFixture<SardisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SardisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SardisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
