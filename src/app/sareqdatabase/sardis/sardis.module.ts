import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SardisPageRoutingModule } from './sardis-routing.module';

import { SardisPage } from './sardis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SardisPageRoutingModule
  ],
  declarations: [SardisPage]
})
export class SardisPageModule {}
