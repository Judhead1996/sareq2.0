import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SardisPage } from './sardis.page';

const routes: Routes = [
  {
    path: '',
    component: SardisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SardisPageRoutingModule {}
