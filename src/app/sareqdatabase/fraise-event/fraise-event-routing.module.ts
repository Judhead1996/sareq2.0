import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FraiseEventPage } from './fraise-event.page';

const routes: Routes = [
  {
    path: '',
    component: FraiseEventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FraiseEventPageRoutingModule {}
