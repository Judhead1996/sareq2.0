import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FraiseEventPage } from './fraise-event.page';

describe('FraiseEventPage', () => {
  let component: FraiseEventPage;
  let fixture: ComponentFixture<FraiseEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FraiseEventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FraiseEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
