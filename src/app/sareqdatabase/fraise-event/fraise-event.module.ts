import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FraiseEventPageRoutingModule } from './fraise-event-routing.module';

import { FraiseEventPage } from './fraise-event.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FraiseEventPageRoutingModule
  ],
  declarations: [FraiseEventPage]
})
export class FraiseEventPageModule {}
