import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SareqTransPage } from './sareq-trans.page';

describe('SareqTransPage', () => {
  let component: SareqTransPage;
  let fixture: ComponentFixture<SareqTransPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SareqTransPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SareqTransPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
