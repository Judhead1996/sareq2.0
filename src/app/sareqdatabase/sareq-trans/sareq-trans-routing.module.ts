import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SareqTransPage } from './sareq-trans.page';

const routes: Routes = [
  {
    path: '',
    component: SareqTransPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SareqTransPageRoutingModule {}
