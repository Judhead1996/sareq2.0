import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SareqTransPageRoutingModule } from './sareq-trans-routing.module';

import { SareqTransPage } from './sareq-trans.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SareqTransPageRoutingModule
  ],
  declarations: [SareqTransPage]
})
export class SareqTransPageModule {}
