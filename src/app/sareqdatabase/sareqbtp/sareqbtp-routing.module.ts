import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SareqbtpPage } from './sareqbtp.page';

const routes: Routes = [
  {
    path: '',
    component: SareqbtpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SareqbtpPageRoutingModule {}
