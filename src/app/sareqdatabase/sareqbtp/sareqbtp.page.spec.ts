import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SareqbtpPage } from './sareqbtp.page';

describe('SareqbtpPage', () => {
  let component: SareqbtpPage;
  let fixture: ComponentFixture<SareqbtpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SareqbtpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SareqbtpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
