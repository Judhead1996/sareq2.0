import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SareqbtpPageRoutingModule } from './sareqbtp-routing.module';

import { SareqbtpPage } from './sareqbtp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SareqbtpPageRoutingModule
  ],
  declarations: [SareqbtpPage]
})
export class SareqbtpPageModule {}
