import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FactureDefinitivePageRoutingModule } from './facture-definitive-routing.module';

import { FactureDefinitivePage } from './facture-definitive.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FactureDefinitivePageRoutingModule
  ],
  declarations: [FactureDefinitivePage]
})
export class FactureDefinitivePageModule {}
