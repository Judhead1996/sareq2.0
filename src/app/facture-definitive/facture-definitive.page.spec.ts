import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FactureDefinitivePage } from './facture-definitive.page';

describe('FactureDefinitivePage', () => {
  let component: FactureDefinitivePage;
  let fixture: ComponentFixture<FactureDefinitivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactureDefinitivePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FactureDefinitivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
