import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureDefinitivePage } from './facture-definitive.page';

const routes: Routes = [
  {
    path: '',
    component: FactureDefinitivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FactureDefinitivePageRoutingModule {}
