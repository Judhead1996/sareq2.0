import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FicheClientPageRoutingModule } from './fiche-client-routing.module';

import { FicheClientPage } from './fiche-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FicheClientPageRoutingModule
  ],
  declarations: [FicheClientPage]
})
export class FicheClientPageModule {}
