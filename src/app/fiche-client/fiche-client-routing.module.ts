import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheClientPage } from './fiche-client.page';

const routes: Routes = [
  {
    path: '',
    component: FicheClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FicheClientPageRoutingModule {}
