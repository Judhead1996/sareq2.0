import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FicheClientPage } from './fiche-client.page';

describe('FicheClientPage', () => {
  let component: FicheClientPage;
  let fixture: ComponentFixture<FicheClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FicheClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
