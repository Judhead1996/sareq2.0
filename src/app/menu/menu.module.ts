import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPageRoutingModule } from './menu-routing.module';

import { MenuPage } from './menu.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      { 
        path: 'home',
         loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'fraise-event',
        loadChildren: () => import('../sareqdatabase/fraise-event/fraise-event.module').then( m => m.FraiseEventPageModule)
      },
      {
        path: 'sardis',
        loadChildren: () => import('../sareqdatabase/sardis/sardis.module').then( m => m.SardisPageModule)
      },
      {
        path: 'sareq-trans',
        loadChildren: () => import('../sareqdatabase/sareq-trans/sareq-trans.module').then( m => m.SareqTransPageModule)
      },
      {
        path: 'sareqbtp',
        loadChildren: () => import('../sareqdatabase/sareqbtp/sareqbtp.module').then( m => m.SareqbtpPageModule)
      },
      {
        path: 'fiche-client',
        loadChildren: () => import('../fiche-client/fiche-client.module').then( m => m.FicheClientPageModule)
      },
      {
        path: 'aggrement',
        loadChildren: () => import('../aggrement/aggrement.module').then( m => m.AggrementPageModule)
      },
      {
        path: 'facture-definitive',
        loadChildren: () => import('../facture-definitive/facture-definitive.module').then( m => m.FactureDefinitivePageModule)
      },
      {
        path: 'facture-proformat',
        loadChildren: () => import('../facture-proformat/facture-proformat.module').then( m => m.FactureProformatPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPageRoutingModule,
    RouterModule.forChild(routes)

  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
