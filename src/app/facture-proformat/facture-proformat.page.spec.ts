import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FactureProformatPage } from './facture-proformat.page';

describe('FactureProformatPage', () => {
  let component: FactureProformatPage;
  let fixture: ComponentFixture<FactureProformatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactureProformatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FactureProformatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
