import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FactureProformatPageRoutingModule } from './facture-proformat-routing.module';

import { FactureProformatPage } from './facture-proformat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FactureProformatPageRoutingModule
  ],
  declarations: [FactureProformatPage]
})
export class FactureProformatPageModule {}
