import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureProformatPage } from './facture-proformat.page';

const routes: Routes = [
  {
    path: '',
    component: FactureProformatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FactureProformatPageRoutingModule {}
