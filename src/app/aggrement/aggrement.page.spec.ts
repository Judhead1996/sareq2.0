import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AggrementPage } from './aggrement.page';

describe('AggrementPage', () => {
  let component: AggrementPage;
  let fixture: ComponentFixture<AggrementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AggrementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AggrementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
