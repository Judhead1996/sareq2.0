import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AggrementPageRoutingModule } from './aggrement-routing.module';

import { AggrementPage } from './aggrement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AggrementPageRoutingModule
  ],
  declarations: [AggrementPage]
})
export class AggrementPageModule {}
