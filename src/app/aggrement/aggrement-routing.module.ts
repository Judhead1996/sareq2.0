import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AggrementPage } from './aggrement.page';

const routes: Routes = [
  {
    path: '',
    component: AggrementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AggrementPageRoutingModule {}
